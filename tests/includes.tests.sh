
# if -ti (tty allocated and interactive) nohup ./test.sh will not work completely since it denies tty allocation.
interactive="-ti"
# interactive=""


scripts=$(dirname $(pwd))/src
export PATH=$scripts:$PATH
docker_run_common="docker run --rm $interactive -v `pwd`:/home/mim:rw -w /home/mim"


function test_mi() {
  img="$1"
  cmd="$2"

  [ -n "`docker image ls $img | grep -v REPOSITORY`" ] && docker rmi $img
  docker import ${img}.tar.gz $img
  $docker_run_common -u `id -u`:`id -g` -e HOME=/home/mim $img $cmd
}

docker_run="$docker_run_common -v $scripts:/home/mim/bin:ro --cap-add=SYS_PTRACE"