#!/bin/bash

set -xe

. includes.tests.sh

date

mim -l "$docker_run base/archlinux" \
   -p 'pacman --noconfirm -Sy --needed strace python tar sudo' \
   -p 'pacman --noconfirm -Sy --needed gcc' \
   -m 'gcc -v' -N arch-gcc-v-example "$@"

date

test_mi arch-gcc-v-example "gcc -v"

date
