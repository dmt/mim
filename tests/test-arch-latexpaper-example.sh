#!/bin/bash

set -xe

. includes.tests.sh

date
[ -d example-latexpaper ] && git -C example-latexpaper pull || git clone https://gitlab.com/dmt/example-latexpaper.git
date

mim -l "$docker_run base/archlinux" \
   -p 'pacman --noconfirm -Sy --needed strace python tar sudo' \
   -p 'pacman --noconfirm -Sy --needed texlive-bin texlive-core texlive-publishers git' \
   -m './latexpaper-example.sh' -N arch-latexpaper-example1 "$@"

date

test_mi arch-latexpaper-example1 ./latexpaper-example.sh

date
