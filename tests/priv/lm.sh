#!/bin/bash

set -ex

export NINJA_STATUS='[%c %r %s/%t %p] '

pushd lm
b=b
rm -rf $b
mkdir -p $b
pushd $b

declare -A t
date
lscpu

t[g]=`date +%s`
../genflags.py gcc dbg openmpi openblas > flags.mk
../configure.py
t[n]=`date +%s`
ninja tbe
t[f]=`date +%s`

printf "configuring: %ds, compiling: %ds\n" $((${t[n]} - ${t[g]})) $((${t[f]} - ${t[n]}))


