#!/bin/bash

set -xe

. includes.tests.sh

date

./mim -l "$docker_run base/archlinux" \
   -p 'pacman --noconfirm -Sy --needed strace python tar sudo' \
   -p 'pacman --noconfirm -Sy --needed vtk gcc ninja lapack blas mpfr freetype2 glew libglvnd libx11 libxt libjpeg-turbo libpng libtiff libharu' \
   -m './v3d.sh' -N arch-v3d "$@"

date

test_mi arch-v3d ./v3d.sh

date
