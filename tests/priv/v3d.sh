#!/bin/bash

set -ex

export NINJA_STATUS='[%c %r %s/%t %p] '

pushd v3d
export CPATH=/usr/include/vtk:/usr/include/vtk-8.0
ninja clean
ninja v3d



