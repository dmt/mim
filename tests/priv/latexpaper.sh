#!/bin/bash

set -ex

pushd latexpaper

rm -f *.aux *.log *.out *.bgl

for p in paper supplementary_material; do
    lualatex --halt-on-error ${p}.tex
    bibtex ${p}.aux
    lualatex --halt-on-error ${p}.tex
    lualatex --halt-on-error ${p}.tex
done

popd






