#!/bin/bash

set -xe

. includes.tests.sh

date

./mim -l "$docker_run opensuse:tumbleweed" \
   -p 'zypper -n --gpg-auto-import-keys in strace python tar sudo' \
   -p 'zypper -n --gpg-auto-import-keys in vtk-devel gcc-c++ ninja lapack-devel blas-devel mpfr-devel' \
   -m './v3d.sh' -N suse-v3d "$@"

#./mim -l "docker run --rm -ti --cap-add=SYS_PTRACE -v `pwd`:/home/mim:rw -w /home/mim susevtk" \
#    -m './v3d.sh' -N suse-v3d

date

test_mi suse-v3d ./v3d.sh

date
