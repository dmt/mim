#!/bin/bash

./test-arch-dlpoly.sh   --make-full-image-too
./test-arch-v3d.sh      --make-full-image-too
./test-suse-dlpoly.sh   --make-full-image-too
./test-suse-lm.sh       --make-full-image-too
./test-suse-v3d.sh      --make-full-image-too
./test-fedora-lm.sh     --make-full-image-too
./test-fedora-dlpoly.sh --make-full-image-too
./test-ubuntu-dlpoly.sh --make-full-image-too


