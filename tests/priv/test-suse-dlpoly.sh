#!/bin/bash

set -xe

. includes.tests.sh

date

./mim -l "$docker_run opensuse:tumbleweed" \
   -p 'zypper -n in strace python tar sudo' \
   -p 'zypper -n in openmpi-devel lua-lmod glibc-locale gcc-fortran cmake make' \
   -m './dlpoly-suse.sh' -N suse-dlpoly "$@"

date

test_mi suse-dlpoly ./dlpoly-suse.sh

date
