#!/bin/bash

set -xe

. includes.tests.sh

date

./mim -l "$docker_run opensuse:tumbleweed" \
    -p 'zypper -n ar https://download.opensuse.org/repositories/home:/pashov_d:/branches:/home:/NfG84:/cluster/openSUSE_Tumbleweed_standard/home:pashov_d:branches:home:NfG84:cluster.repo' \
    -p 'zypper -n --gpg-auto-import-keys in strace python tar sudo' \
    -p 'zypper -n --gpg-auto-import-keys in libxc-devel openmpi-devel Modules gcc-fortran gcc-c++ libscalapack2-openmpi-devel libopenblas_openmp-devel fftw3-openmp-devel ninja tcsh glibc-locale git sed grep make' \
    -m './lm-suse.sh' -N suse-lm "$@"

# ./mim -l "docker run --rm -ti --cap-add=SYS_PTRACE -v `pwd`:/home/w:rw -w /home/w suse-lm-full" \
#    -m './lm-suse.sh' -N suse-lm

date

test_mi suse-lm ./lm-suse.sh

date
