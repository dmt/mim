#!/bin/bash

set -xe

. includes.tests.sh

date

./mim -l "$docker_run fedora" \
   -p 'dnf -y in strace python sudo' \
   -p 'dnf -y in openmpi-devel openblas-devel scalapack-openmpi-devel fftw-devel libxc-devel ninja-build git' \
   -m './lm-fedora.sh' -N fedora-lm "$@"

date

test_mi fedora-lm ./lm-fedora.sh

date
