#!/bin/bash

set -xe

. includes.tests.sh

date

./mim -l "$docker_run base/archlinux" \
   -p 'pacman --noconfirm -Sy --needed strace python tar sudo' \
   -p 'pacman --noconfirm -Sy --needed openmpi gcc-fortran cmake make' \
   -m './dlpoly.sh' -N arch-dlpoly "$@"

date

test_mi arch-dlpoly ./dlpoly.sh

date
