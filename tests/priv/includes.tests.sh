
# if -ti (tty allocated and interactive) nohup ./test.sh will not work completely since it denies tty allocation.
interactive="-ti"
# interactive=""

docker_run_common="docker run --rm $interactive -v `pwd`:/home/mim:rw -w /home/mim"


function test_mi() {
  img=$1
  cmd=$2

  [ -n "`docker image ls $img | grep -v REPOSITORY`" ] && docker rmi $img
  docker import ${img}.tar.gz $img
  $docker_run_common -u `id -u`:`id -g` -e HOME=/home/mim $img $cmd
}

docker_run="$docker_run_common --cap-add=SYS_PTRACE"