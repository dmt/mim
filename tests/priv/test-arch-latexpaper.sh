#!/bin/bash

set -xe

. includes.tests.sh

date

./mim -l "$docker_run base/archlinux" \
   -p 'pacman --noconfirm -Sy --needed strace python tar sudo' \
   -p 'pacman --noconfirm -Sy --needed texlive-bin texlive-core' \
   -m './latexpaper.sh' -N arch-latexpaper "$@"

date

test_mi arch-latexpaper ./latexpaper.sh

date
