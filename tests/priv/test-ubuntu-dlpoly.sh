#!/bin/bash

set -xe

. includes.tests.sh

date

./mim -l "$docker_run ubuntu" \
   -p 'apt-get -y update' \
   -p 'apt-get -y install strace python tar sudo' \
   -p 'apt-get -y install libopenmpi-dev gfortran cmake make' \
   -m './dlpoly-ubuntu.sh' -N ubuntu-dlpoly "$@"

date

test_mi ubuntu-dlpoly ./dlpoly-ubuntu.sh

date
