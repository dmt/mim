#!/bin/bash

set -xe

. includes.tests.sh

date

./mim -l "$docker_run fedora" \
   -p 'dnf -y in strace python sudo' \
   -p 'dnf -y in openmpi-devel cmake file' \
   -m './dlpoly-fedora.sh' -N fedora-dlpoly "$@"

date

test_mi fedora-dlpoly ./dlpoly-fedora.sh

date
