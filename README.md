# Mimimal-Image Maker

Make the most minimal container image for any task at hand.


## Synopsis
mim [-h] -l "launcher command" [-p "preparatory command"] -m "moulding command" [-n] [-N output_name] [-k] [-s "/bin/bash -l"]

## Description

*mim* moulds an OS image to a given *moulding command*. The full image is prepared by running one or more *preparatory commands* into a container launched with the *launcher command*. All files accessed by the *mounding command* are traced, organised appropriately and wrapped in a minimal image.

A level of abstraction is applied to allow portability to different containerisation technologies (from chroot up)

To avoid wasting precious developing time by starting failed builds from the beginning a recovery shell is included (defaulting to "/bin/bash -l"). If manual recovery is performed the build will continue on exit. If any of the following commands fail the shell will be invoked again. Returning error from the shell (e.g. by typing 'exit -1') will quit the build entirely.

## Options

-l, --launcher "launcher command"
  command to launch the host container

-p, --prep "preparatory command"
  preparatory command (its dependencies will not be included in the minimal image)
  may be used multiple times, the result will be an ordered list of commands

-m, --mould "moulding command"
  the target command for which a minimal image will be made

-n, --noexec
  do not execute anything, only print commands

-N, --name output_name
  basename of output archive

-k, --keep-traces
  do not remove intermediary trace files

-s, --recovery-shell "/bin/bash -l"
  on failure launch the specified interactive shell

## Examples
The following command will produce a minimal image in docker capable of runing the command `gcc -v`:

    > ./mim -l "docker run --rm -ti --cap-add=SYS_PTRACE -v `pwd`:/home/mim:rw -w /home/mim base/archlinux" \
        -p 'pacman --noconfirm -Sy --needed strace python tar sudo' \
        -p 'pacman --noconfirm -Sy --needed gcc' \
        -m 'gcc -v' \
        -N archlinux-gcc-v
    ...
It produced an archive named *archlinux-gcc-v.tar.gz* of size 2.5MiB. Its contents are only:

    > tar -tf archlinux-gcc-v.tar.gz
    etc/
    etc/ld.so.cache
    lib
    usr/
    usr/lib/
    usr/lib/ld-linux-x86-64.so.2
    usr/lib/ld-2.26.so
    usr/sbin
    usr/bin/
    usr/bin/gcc
    usr/lib/gcc/
    usr/lib/gcc/x86_64-pc-linux-gnu/
    usr/lib/gcc/x86_64-pc-linux-gnu/7.2.1/
    usr/lib/gcc/x86_64-pc-linux-gnu/7.2.1/lto-wrapper
    tmp/
    lib64
    usr/lib/libc.so.6
    usr/lib/libc-2.26.so
    usr/lib/libm-2.26.so
    usr/lib/libm.so.6

The image can be tested for example with docker by importing it and running the original command `gcc -v`.

    > docker import archlinux-gcc-v.tar.gz archlinux-gcc-v:20171211
    > docker run --rm -ti -u `id -u`:`id -g` -v `pwd`:/home/mim:rw -w /home/mim archlinux-gcc-v:20171211 gcc -v
    Using built-in specs.
    COLLECT_GCC=gcc
    COLLECT_LTO_WRAPPER=/usr/lib/gcc/x86_64-pc-linux-gnu/7.2.1/lto-wrapper
    Target: x86_64-pc-linux-gnu
    Configured with: /build/gcc/src/gcc/configure --prefix=/usr --libdir=/usr/lib --libexecdir=/usr/lib --mandir=/usr/share/man --infodir=/usr/share/info --with-bugurl=https://bugs.archlinux.org/ --enable-languages=c,c++,ada,fortran,go,lto,objc,obj-c++ --enable-shared --enable-threads=posix --enable-libmpx --with-system-zlib --with-isl --enable-__cxa_atexit --disable-libunwind-exceptions --enable-clocale=gnu --disable-libstdcxx-pch --disable-libssp --enable-gnu-unique-object --enable-linker-build-id --enable-lto --enable-plugin --enable-install-libiberty --with-linker-hash-style=gnu --enable-gnu-indirect-function --enable-multilib --disable-werror --enable-checking=release --enable-default-pie --enable-default-ssp
    Thread model: posix
    gcc version 7.2.1 20171128 (GCC)

## Acknowledgements
dl_poly_4.08 tests thanks to Alin Elena

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 only,
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Copyright (c) 2016-2017 Dimitar Pashov
